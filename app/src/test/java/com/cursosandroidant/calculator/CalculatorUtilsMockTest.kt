package com.cursosandroidant.calculator

import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CalculatorUtilsMockTest {
    @Mock
    lateinit var operations: Operations

    @Mock
    lateinit var listener: OnResolveListener

    private lateinit var calculatorUtils: CalculatorUtils


    @Before
    fun setup() {
        calculatorUtils = CalculatorUtils(operations, listener)
    }


    @Test
    fun calculator_callCheckOrResolve_noReturn() {
        val operation = "-5*2.5"
        val isResolve = true
        calculatorUtils.checkOrResolve(operation, isResolve)
        verify(operations).tryResolve(operation, isResolve, listener)
    }

    @Test
    fun calculator_callOperator_validSub_noReturn() {
        val operator = "-"
        val operation = "4+"
        var isCorrect = false
        calculatorUtils.addOperator(operator, operation) {
            isCorrect = true
        }
        assertTrue(isCorrect)
    }

    @Test
    fun calculator_callOperator_invalidSub_noReturn() {
        val operator = "-"
        val operation = "4+-"
        var isCorrect = true
        calculatorUtils.addOperator(operator, operation) {
            isCorrect = false
        }
        assertTrue(isCorrect)
    }

    @Test
    fun calculator_callAddPoint_firstPoint_noReturn() {
        val operation = "3x2"
        var isCorrect = false
        calculatorUtils.addPoint(operation) {
            isCorrect = true
        }
        assertTrue(isCorrect)
        verifyNoInteractions(operations)
    }

    @Test
    fun calculator_callAddPoint_secondPoint_noReturn() {
        val operation = "3.5x2"
        val operator = "x"
        var isCorrect = false
        `when`(operations.getOperator(operation)).thenReturn("x")
        `when`(operations.divideOperation(operator, operation)).thenReturn(arrayOf("3.5", "2"))
        calculatorUtils.addPoint(operation) {
            isCorrect = true
        }
        assertTrue(isCorrect)
        verify(operations).getOperator(operation)
        verify(operations).divideOperation(operator, operation)
    }
}